package jp.hazuki.yuzubrowser.browser.ui.browser

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import jp.hazuki.yuzubrowser.browser.common.Constants
import jp.hazuki.yuzubrowser.browser.data.repository.BrowserRepository
import jp.hazuki.yuzubrowser.browser.utils.rx.SchedulerProvider
import jp.hazuki.yuzubrowser.legacy.tab.manager.MainTabData

class BrowserViewModel(
    private val compositeDisposable: CompositeDisposable,
    private val browserRepository: BrowserRepository

) : ViewModel() {
    val deepLinkUrl = MutableLiveData<HashMap<String,Any>>()


    fun onShareLinkClicked(branchKey: String, tab: MainTabData) {
        compositeDisposable.addAll(
            browserRepository.generateToken(
                branchKey, tab.url
            ).subscribeOn(Schedulers.io())
                .subscribe({
                    val map = HashMap<String,Any>()
                    map[Constants.URL] = it.url
                    map[Constants.DATA] = tab
                    deepLinkUrl.postValue(map)
                }, {

                })
        )
    }


    override fun onCleared() {
        compositeDisposable.dispose()
        super.onCleared()
    }
}

