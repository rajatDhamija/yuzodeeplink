package jp.hazuki.yuzubrowser.browser.data

import io.reactivex.Single
import jp.hazuki.yuzubrowser.browser.data.request.URLRequest
import jp.hazuki.yuzubrowser.browser.data.response.URLResponse
import retrofit2.http.Body
import retrofit2.http.POST
import javax.inject.Singleton

@Singleton
interface NetworkService {

    @POST(Endpoints.GENERATE_URL)
    fun generateToken(
        @Body urlRequest: URLRequest
    ): Single<URLResponse>
}
