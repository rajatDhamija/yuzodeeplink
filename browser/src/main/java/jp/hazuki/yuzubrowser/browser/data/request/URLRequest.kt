package jp.hazuki.yuzubrowser.browser.data.request

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import jp.hazuki.yuzubrowser.browser.common.Constants

data class URLRequest(
    @Expose
    @SerializedName(Constants.BRANCH_KEY)
    var branch_key: String,

    @Expose
    @SerializedName(Constants.DATA)
    var data: Data
) {
    data class Data(
        @Expose
        @SerializedName(Constants.SHARED_URL)
        var shared_url: String
    )
}
