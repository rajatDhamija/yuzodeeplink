package jp.hazuki.yuzubrowser.browser.data.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import jp.hazuki.yuzubrowser.browser.common.Constants

data class URLResponse(
    @Expose
    @SerializedName(Constants.URL)
    var url: String
)
