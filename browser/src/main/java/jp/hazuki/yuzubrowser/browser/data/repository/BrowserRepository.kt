package jp.hazuki.yuzubrowser.browser.data.repository

import io.reactivex.Single
import jp.hazuki.yuzubrowser.browser.data.NetworkService
import jp.hazuki.yuzubrowser.browser.data.request.URLRequest
import jp.hazuki.yuzubrowser.browser.data.response.URLResponse
import javax.inject.Inject

class BrowserRepository @Inject constructor(
    private val networkService: NetworkService
) {
    fun generateToken(
        branchKey: String,
        sharedUrl: String
    ): Single<URLResponse> =
        networkService.generateToken(URLRequest(branchKey, URLRequest.Data(sharedUrl)))
}
