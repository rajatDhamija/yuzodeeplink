package jp.hazuki.yuzubrowser.browser.common

object Constants {
    const val BASE_URL = "https://api2.branch.io"
    const val BRANCH_KEY = "branch_key"
    const val DATA = "data"
    const val SHARED_URL = "shared_url"
    const val URL = "url"
    const val BRANCH_TEST_KEY = "key_test_onQ4MesD0OZMmkgrNk7IFpjdAtpeqSFE"
}
